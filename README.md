Bot server for telegram
=================================================

Server for bots of telegram. Can be used on heroku.


Where to get the token?
=====================================================

You can create and get the token through the Telegram **BotFather**.

Start a conversation with him and see how to get the token and create your bot.

There are some online documents for this as well.


Download code
=====================================================

To download the source code, we use git and the current repository address. The repository address is:

```bash
https://bitbucket.org/psycho_mantys/heroku-python-telegram-bots.git
```

You can use any git graphical client, but if you have it in a command-line environment, you can do:

```bash
git clone https://bitbucket.org/psycho_mantys/heroku-python-telegram-bots.git
```

Create development environment
=====================================================

To configure the development environment, use `pipenv`.

For more details, see the `pipenv` documentation.

Installing development dependencies
--------------------------------------------------------

To manage the development dependencies, we will use `pipenv`.

`Pipenv` should executed only once at startup after downloading the repository code. To do this, in a command line environment and whereas, you must type:

```bash
cd heroku-python-telegram-bots
pipenv install
```

This code will create an environment for the source code with the python tool.

If your system's python is not by default the one used in the project(in the case, python 3), you must specify the python version in `pipenv`, running in place of the lines above the following:

```bash
cd heroku-python-telegram-bots
pipenv install --two
```

Preferably, use the newer version of python.

Entering the python virtual environment
--------------------------------------------------------

To enable this environment and use the tools in this environment, run:

```bash
pipenv shell
```

Exiting the python virtual environment
--------------------------------------------------------

When you finish running and installing dependencies in this environment, simply exit the shell.

Use the Server
=====================================================

To use the bot, you must run it and keep it running on any machine.

To add bots, add the directory or the submodule for the git repository, for example:

```bash
git submodule add --force https://bitbucket.org/psycho_mantys/psy_rpg_bot psy_rpg_bot
git submodule add --force https://bitbucket.org/psycho_mantys/fortunebr_bot fortunebr_bot
```

Set this environment to start-up the server:

```bash
export BOT_TOKENS='KKKAAAAAAAAAq32133213213aaaa DADAFVFFGDFRSDIiisidsad9888ffff7'
export BOT_FILE_NAMES='psy_rpg_bot/__init__.py fortunebr_bot/__init__.py'
export PORT=9090
export WEBHOOK_URI_TMPL='https://marvelous-apache-3434.herokuapp.com/{TOKEN}'
```

On heroku, for example:

```bash
heroku config:set BOT_TOKENS='KKKAAAAAAAAAq32133213213aaaa DADAFVFFGDFRSDIiisidsad9888ffff7'
heroku config:set BOT_FILE_NAMES='psy_rpg_bot/__init__.py fortunebr_bot/__init__.py'
heroku config:set PORT='9090'
heroku config:set WEBHOOK_URI_TMPL='https://marvelous-apache-3434.herokuapp.com/{TOKEN}'
```

Each file on BOT\_FILE\_NAMES must have a token on BOT\_TOKENS.

Each file on BOT\_FILE\_NAMES must have a function `def create_from_bot(bot)`, who set the bot, but not block.

And, execute you server or push to heroku:

```bash
git commit
git push heroku
```

Final notes
===============================================

Update submodules:

```bash
git submodule update --remote --merge
```

