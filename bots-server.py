import logging
import importlib
import os
import sys
import copy
from queue import Queue
from contextlib import contextmanager

import cherrypy
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Dispatcher

sys.setrecursionlimit(5000)

@contextmanager
def add_to_path(p):
	import sys
	old_paths=copy.copy(sys.path)
	old_modules=list(sys.modules.keys())
	old_cwd=os.getcwd()

	sys.path.insert(0, p)
	importlib.invalidate_caches()
	os.chdir(p)

	try:
		yield
	finally:
		for k in sys.path:
			if not k in old_paths:
				sys.path.remove(k)

		for k in list(sys.modules.keys()):
			if not k in old_modules:
				sys.modules.pop(k,None)

		importlib.invalidate_caches()
		os.chdir(old_cwd)

def path_import(absolute_path):
	absolute_path=os.path.abspath(absolute_path)
	'''implementation taken from https://docs.python.org/3/library/importlib.html#importing-a-source-file-directly'''
	with add_to_path(os.path.dirname(absolute_path)):
		spec=importlib.util.spec_from_file_location(absolute_path, absolute_path)
		module=importlib.util.module_from_spec(spec)
		spec.loader.exec_module(module)
		return module


class Simple_website(object):
	def __init__(self):
		self.BOT_USERNAMES=[]

	@cherrypy.expose
	def index(self):
		ret="""
<H1>Welcome!</H1>
<p>Server for Multiple telegram bots. Made by <a href="https://t.me/psycho_mantys">@psycho_mantys</a></p>
<p>Bots online in server:</p>
"""
		for name in self.BOT_USERNAMES:
			ret+='<p><a href="https://t.me/{botname}">@{botname}</a>'.format(botname=name)
		return ret


class Bot_app(object):
	exposed=True

	def __init__(self, TOKEN, WEBHOOK_URI_TMPL, bot_module):
		super(Bot_app, self).__init__()
		self.TOKEN=TOKEN
		self.WEBHOOK_URI_TMPL=WEBHOOK_URI_TMPL
		self.bot=telegram.Bot(self.TOKEN)

		try:
			self.bot.set_webhook(WEBHOOK_URI_TMPL.format(
				TOKEN=self.TOKEN
			))
		except:
			#raise RuntimeError("Failed to set the webhook")
			print("Failed to set the webhook")

		self.app=bot_module.create_from_bot(self.bot)

	@cherrypy.tools.json_in()
	def POST(self, *args, **kwargs):
		update=cherrypy.request.json
		update=telegram.Update.de_json(update, self.bot)
		self.app.dispatcher.process_update(update)


if __name__ == "__main__":
	PORT=os.environ['PORT']
	WEBHOOK_URI_TMPL=os.environ["WEBHOOK_URI_TMPL"]

	BOT_FILE_NAMES=os.environ["BOT_FILE_NAMES"].split(' ')
	BOT_TOKENS=os.environ["BOT_TOKENS"].split(' ')

	# Enable logging
	logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
		level=logging.INFO)
	logger=logging.getLogger(__name__)

	# Set up the cherrypy configuration
	cherrypy.config.update({'server.socket_host': '0.0.0.0', })
	cherrypy.config.update({'server.socket_port': int(PORT), })

	site=Simple_website()
	cherrypy.tree.mount(site, "/")

	for bot_app_file, TOKEN in zip(BOT_FILE_NAMES, BOT_TOKENS):
		logger.warning("Start Bot: {}, Token: {}".format(bot_app_file, TOKEN))
		bot_module=path_import(bot_app_file)
		bot_app=Bot_app(TOKEN, WEBHOOK_URI_TMPL, bot_module)
		cherrypy.tree.mount(
			bot_app,
			"/{}".format(TOKEN),
			{'/': {'request.dispatch': cherrypy.dispatch.MethodDispatcher()}}
		)
		if hasattr(bot_app.app, "TELEGRAM_BOT_USERNAME"):
			site.BOT_USERNAMES.append(bot_app.app.TELEGRAM_BOT_USERNAME)
		if hasattr(bot_app.app, "config") and hasattr(bot_app.app.config, "TELEGRAM_BOT_USERNAME"):
			site.BOT_USERNAMES.append(bot_app.app.config.TELEGRAM_BOT_USERNAME)

	cherrypy.engine.start()

